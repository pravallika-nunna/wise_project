import tkinter as tk
from tkinter import ttk

def on_dropdown_select(event):
    selected_item = combo_var.get()
    result_label.config(text=f"Selected: {selected_item}")

# Create the main tkinter window
root = tk.Tk()
root.title("Dropdown Menu Example")

# Create a label
label = ttk.Label(root, text="Select an item:")
label.pack(pady=10)

# Create a Combobox
combo_var = tk.StringVar()
dropdown = ttk.Combobox(root, textvariable = combo_var)
dropdown['values'] = ("Option 1", "Option 2", "Option 3", "Option 4")
dropdown.pack()

# Bind an event handler to the Combobox
dropdown.bind("<<ComboboxSelected>>", on_dropdown_select)

# Create a label to display the selected item
result_label = ttk.Label(root, text="")
result_label.pack(pady=10)

# Start the tkinter main loop
root.mainloop()
