import tkinter as tk
from tkinter import *
from PIL import Image,ImageTk

class Home():
    def __init__(self,root):
        self.root=root
        self.bg_img = Image.open('bg1.png')
        self.bg_img = self.bg_img.resize((1500,2000))
        self.bg_img = ImageTk.PhotoImage(self.bg_img)
        self.bg_lbl = Label(root,image = self.bg_img)
        self.bg_lbl.place(x=0,y=0)
        self.bg_lb1 = Label(root,text = "Recipe Generator",)
        self.bg_lb1.place(x=0,y=0)

# Creating a window
root = tk.Tk()
root.geometry('1100x650+450+320')
root.title("Recipe Generator")
home = Home(root)

# Create a list to store ingredients
ingredients = []

#
def find_matching_recipe():
    pass

# Function to add an ingredient to the list
def add_ingredient():
    ingredient = ingredient_entry.get()
    if isinstance(ingredient,str):
        ingredients.append(ingredient)
        ingredient_entry.delete(0, tk.END)
        update_ingredient_list()
    else:
        tk.messagebox.showerror("Error", "Please enter an ingredient.")


# Function to update the ingredient list
def update_ingredient_list():
    ingredient_listbox.delete(0, tk.END)
    for ingredient in ingredients:
        ingredient_listbox.insert(tk.END, ingredient)

# Ingredient Entry
ingredient_label = tk.Label(root, text="Enter an Ingredient:")
ingredient_label.pack()
ingredient_entry = tk.Entry(root)
ingredient_entry.pack()



# Function to generate a recipe based on ingredients
def generate_recipe():
    a=tk.Tk()
    a.geometry('550x350+350+220')

    def main_page(self):
        self(main_page).destroy()
        self.Home.destroy()
    
    mainn = Home(root)

    main_page()
    if ingredients:
        recipe = f"Recipe:\n"
        for i, ingredient in enumerate(ingredients, start=1):
            recipe += f"{i}. {ingredient}\n"
        recipe_text.config(state=tk.NORMAL)
        recipe_text.delete(1.0, tk.END)
        recipe_text.insert(tk.END, recipe)
        recipe_text.config(state=tk.DISABLED)
    else:
        tk.messagebox.showwarning("Warning", "Please add ingredients to generate a recipe.")


def find_matching_recipes():
    pass
    #if ingredients

# Buttons
add_button = tk.Button(root, text="Add Ingredient", command=add_ingredient)
generate_button = tk.Button(root, text="Generate Recipe", command=generate_recipe)
add_button.pack()
generate_button.pack()

# Creating a label
label = Label(root, text="Search Here :")
label.place(x=125 , y=10)

# Creating a Browse button
browse_button = Button(root, text="Browse", command=generate_recipe)
browse_button.place(x=340,y=10)

# Create and pack a label
label = Label(root, text="Enter ingredients (comma-separated):")
label.place(x=0 , y=110)

# Create an entry field
entry = Entry(root)
entry.place(x=210,y=110)

# Create a button to trigger recipe search
search_button = Button(root, text="Find Recipes", command=find_matching_recipe)
search_button.place(x=215,y=135)

entry_generate = Entry(root)
entry_generate.place(x=210,y=200)
label = Label(root, text="generate here:")
label.place(x=120 , y=200)
# Generate Recipe Button
generate_button = Button(root, text="Generate Recipe", command=generate_recipe)
generate_button.place(x=205,y=225)

# Recipe Display
recipe_text = Text(root, width=40, height=8)
recipe_text.place(x=120,y=270)

# Create a Text widget to display file contents
text_widget = Text(root, width=15, height=2)
text_widget.place(x=205,y=10)


# Ingredient List
ingredient_list_label = tk.Label(root, text="Ingredients:")
ingredient_list_label.pack()
ingredient_listbox = tk.Listbox(root)
ingredient_listbox.pack()

# Recipe Display
recipe_label = tk.Label(root, text="Generated Recipe:")
recipe_label.pack()
recipe_text = tk.Text(root, height=10, width=30, state=tk.DISABLED)
recipe_text.pack()

# Run the tkinter main loop
root.mainloop()