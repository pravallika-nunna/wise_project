from tkinter import * 
from PIL import Image , ImageTk
import sqlite3

conn = sqlite3.connect("recipe-ingredients.db")

#to insert a image to wizard
class Home():
    def __init__(self,app):
        self.app=app
        self.bg_img=Image.open('bg1.png')
        self.bg_img=self.bg_img.resize((1500,2000))
        self.bg_img=ImageTk.PhotoImage(self.bg_img)
        self.bg_lbl=Label(app,image=self.bg_img)
        self.bg_lbl.place(x=0,y=0)

app = Tk()
app.geometry('650x350+250+200')
app.title("Recipe Generator")
home = Home(app)

# Function to generate a recipe based on ingredients
def generate_recipe():

    def main_page(self):
        self(main_page).destroy()
        self.Home.destroy()
    
    mainn= Home(app)

# Create the main application window
#title 
title=Label(app , text='RECIPE GENERATOR (TELUGU DISHES)',font=('Times New Roman',15,'bold'))
title.place(x=0,y=0)

# Create and configure GUI elements (labels, entry fields, buttons)
label = Label(app, text="Search Here :")
label.place(x=125 , y=50)

def button_click():
    root.config(text="Match Found")
    
# Create a Text widget to display file contents
text_widget = Text(app , width=15, height=1)
text_widget.place(x=205,y=50)

# Recipe Display
root= Label(app)
root.place(x=205,y=75)
   
# Create a Browse button to select a file
browse_button = Button(app, text="Browse", command=button_click)
browse_button.place(x=340,y=50)

# Sample list of recipes with ingredients
receipes = [
     "Pulihora " ,
     "Gongura Pachadi"
]

# Function to find matching recipes based on user input
def find_matching_recipes():
    pass
        
# Function to update the ingredient list
def add_ingredient():
    ingredient_listbox.delete(0, END)
    for ingredient in receipes:
        ingredient_listbox.insert(END, ingredient)

# Ingredient Entry
ingredient_label = Label(app, text="Enter an Ingredient:")
ingredient_label.place(x=90,y=100)
ingredient_entry = Entry(app)
ingredient_entry.place(x=205,y=100)


def random_recipe():
    def main_page(self):
        self(main_page).destroy()
        self.Home.destroy()
    
    mainn= Home(app)
    

#Random recipe generator
random_ = Button(app,text = "Random Recipe",command=random_recipe)
random_.place(x=200,y=200)
random_recipe_label = Label(app,text = "Random Recipe for you \n 1.")

# Buttons
add_button = Button(app, text="show recipes", command=add_ingredient)
generate_button = Button(app, text="Generate Recipe", command=generate_recipe)
add_button.place(x=200,y=125)
generate_button.place(x=200,y=155)

# Ingredient List
ingredient_list_label = Label(app, text="receipes:")
ingredient_list_label.place(x=485,y=5)
ingredient_listbox = Listbox(app)
ingredient_listbox.place(x=450,y=30)

# Create a label to display the result
app.mainloop()